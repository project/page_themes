<?php

namespace Drupal\page_themes;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for page_themes.
 */
class PageThemesTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
