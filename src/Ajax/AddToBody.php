<?php
/**
* AddToBody.php contains AddToBody class 
* Defines custom ajax command for set value in Body by ajax
**/ 
namespace Drupal\page_themes\Ajax;
use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Database\Connection;

/**
 * Class ExtendCommand.
 */
class AddToBody implements CommandInterface {
   /**
   * A CSS selector string.
   *
   * If the command is a response to a request from an #ajax form element then
   * this value can be NULL.
   *
   * @var string
   */
  protected $selector;

  /**
   * The Page Theme id.
   *
   * @var string
   */
  protected $theme_id;

  /**
     * The database connection.
     *
     * @var Connection
     */
  protected $database;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param string $selector
   *   A jQuery selector.
   * @param string $theme_id
   *   The theme id.
   */
  public function __construct($selector, $theme_id) {
    $this->selector = $selector;
    $this->theme_id = $theme_id;
    $this->database = \Drupal::database();
  }

  public function render() { 
    return [
      'command' => 'AddToBody',
      'selector' => $this->selector,
      'theme' => $this->getTheme(),
    ];  
  }

  private function getTheme() {
    $query = $this->database->select('page_themes_field_data');
    $query->fields('page_themes_field_data', array('theme_code__value'));
    $query->condition('id', $this->theme_id);
    $results = $query->execute()->fetchAll();
    return $results[0]->theme_code__value;
  }
}