<?php

namespace Drupal\page_themes;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\page_themes\Entity\PageThemesInterface;

/**
 * Defines the storage handler class for PageThemes entities.
 *
 * This extends the base storage class, adding required special handling for
 * PageThemes entities.
 *
 * @ingroup page_themes
 */
interface PageThemesStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of PageThemes revision IDs for a specific PageThemes.
   *
   * @param \Drupal\page_themes\Entity\PageThemesInterface $entity
   *   The PageThemes entity.
   *
   * @return int[]
   *   PageThemes revision IDs (in ascending order).
   */
  public function revisionIds(PageThemesInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as PageThemes author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   PageThemes revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\page_themes\Entity\PageThemesInterface $entity
   *   The PageThemes entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(PageThemesInterface $entity);

  /**
   * Unsets the language for all PageThemes with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
