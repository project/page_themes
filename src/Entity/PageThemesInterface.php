<?php

namespace Drupal\page_themes\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining PageThemes entities.
 *
 * @ingroup page_themes
 */
interface PageThemesInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the PageThemes name.
   *
   * @return string
   *   Name of the PageThemes.
   */
  public function getName();

  /**
   * Sets the PageThemes name.
   *
   * @param string $name
   *   The PageThemes name.
   *
   * @return \Drupal\page_themes\Entity\PageThemesInterface
   *   The called PageThemes entity.
   */
  public function setName($name);

  /**
   * Gets the PageThemes creation timestamp.
   *
   * @return int
   *   Creation timestamp of the PageThemes.
   */
  public function getCreatedTime();

  /**
   * Sets the PageThemes creation timestamp.
   *
   * @param int $timestamp
   *   The PageThemes creation timestamp.
   *
   * @return \Drupal\page_themes\Entity\PageThemesInterface
   *   The called PageThemes entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the PageThemes revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the PageThemes revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\page_themes\Entity\PageThemesInterface
   *   The called PageThemes entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the PageThemes revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the PageThemes revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\page_themes\Entity\PageThemesInterface
   *   The called PageThemes entity.
   */
  public function setRevisionUserId($uid);

}
