<?php

namespace Drupal\page_themes;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\page_themes\Entity\PageThemesInterface;

/**
 * Defines the storage handler class for PageThemes entities.
 *
 * This extends the base storage class, adding required special handling for
 * PageThemes entities.
 *
 * @ingroup page_themes
 */
class PageThemesStorage extends SqlContentEntityStorage implements PageThemesStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(PageThemesInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {page_themes_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {page_themes_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(PageThemesInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {page_themes_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('page_themes_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
