<?php

namespace Drupal\page_themes\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\page_themes\Entity\PageThemesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PageThemesController.
 *
 *  Returns responses for PageThemes routes.
 */
class PageThemesController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a PageThemes revision.
   *
   * @param int $page_themes_revision
   *   The PageThemes revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($page_themes_revision) {
    $page_themes = $this->entityTypeManager()->getStorage('page_themes')
      ->loadRevision($page_themes_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('page_themes');

    return $view_builder->view($page_themes);
  }

  /**
   * Page title callback for a PageThemes revision.
   *
   * @param int $page_themes_revision
   *   The PageThemes revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($page_themes_revision) {
    $page_themes = $this->entityTypeManager()->getStorage('page_themes')
      ->loadRevision($page_themes_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $page_themes->label(),
      '%date' => $this->dateFormatter->format($page_themes->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a PageThemes.
   *
   * @param \Drupal\page_themes\Entity\PageThemesInterface $page_themes
   *   A PageThemes object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(PageThemesInterface $page_themes) {
    $account = $this->currentUser();
    $page_themes_storage = $this->entityTypeManager()->getStorage('page_themes');

    $langcode = $page_themes->language()->getId();
    $langname = $page_themes->language()->getName();
    $languages = $page_themes->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $page_themes->label()]) : $this->t('Revisions for %title', ['%title' => $page_themes->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all page themes revisions") || $account->hasPermission('administer page themes entities')));
    $delete_permission = (($account->hasPermission("delete all page themes revisions") || $account->hasPermission('administer page themes entities')));

    $rows = [];
    $vids = $page_themes_storage->revisionIds($page_themes);
    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\page_themes\PageThemesInterface $revision */
      $revision = $page_themes_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $page_themes->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.page_themes.revision', [
            'page_themes' => $page_themes->id(),
            'page_themes_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $page_themes->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.page_themes.translation_revert', [
                'page_themes' => $page_themes->id(),
                'page_themes_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.page_themes.revision_revert', [
                'page_themes' => $page_themes->id(),
                'page_themes_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.page_themes.revision_delete', [
                'page_themes' => $page_themes->id(),
                'page_themes_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['page_themes_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }
}
