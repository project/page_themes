<?php

namespace Drupal\page_themes\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\page_themes\PageThemesStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a PageThemes revision.
 *
 * @ingroup page_themes
 */
class PageThemesRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The PageThemes revision.
   *
   * @var \Drupal\page_themes\Entity\PageThemesInterface
   */
  protected $revision;

  /**
   * The PageThemes storage.
   *
   * @var \Drupal\page_themes\PageThemesStorageInterface
   */
  protected $page_themesStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new NodeRevisionDeleteForm.
   *
   * @param \Drupal\page_themes\PageThemesStorageInterface $page_themesStorage
   *   The node storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(PageThemesStorageInterface $page_themesStorage, Connection $connection, DateFormatterInterface $date_formatter) {
    $this->page_themesStorage = $page_themesStorage;
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('page_themes'),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_themes_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.page_themes.version_history', ['page_themes' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $page_themes_revision = NULL) {
    $this->revision = $this->page_themesStorage->loadRevision($page_themes_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->page_themesStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Page Themes: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage($this->t('Revision from %revision-date of Page Themes %title has been deleted.', ['%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.page_themes.canonical',
       ['page_themes' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {page_themes_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.page_themes.version_history',
         ['page_themes' => $this->revision->id()]
      );
    }
  }

}
