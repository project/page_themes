<?php

namespace Drupal\page_themes\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Page Themes entities.
 *
 * @ingroup page_themes
 */
class PageThemesDeleteForm extends ContentEntityDeleteForm {


}
