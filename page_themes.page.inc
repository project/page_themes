<?php

/**
 * @file
 * Contains page_themes.page.inc.
 *
 * Page callback for Page Themes entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Page Themes templates.
 *
 * Default template: page_themes.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_page_themes(array &$variables) {
  // Fetch Page Themes Entity Object.
  $page_themes = $variables['elements']['#page_themes'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
