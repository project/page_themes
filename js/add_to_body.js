(function ($, Drupal, CKEDITOR) {
    /**
     * Add new custom command.
     */
    Drupal.AjaxCommands.prototype.AddToBody = function (ajax, response, status ) {
      var selector = jQuery(response.selector).attr('id');
      var data = response.theme;
      console.log(selector + " - " + data);
      localStorage.setItem(selector, data);
      localStorage.setItem('data', selector);
    }
})(jQuery, Drupal, CKEDITOR);

jQuery( document ).ajaxStop(function() {
    for(var selector in CKEDITOR.instances) {               // Array of Ckeditor selectors on page
        console.log(selector);
        var storage = localStorage.getItem('data');
        if(localStorage.hasOwnProperty(storage)) {           // Do you have any data for given selector?
            var selectorData = localStorage.getItem(storage);  // Load data
            localStorage.removeItem(storage);                  // Remove data from localStorage
            localStorage.removeItem('data');
            if(selector !== undefined && selector != '' ) {
                CKEDITOR.instances[selector].config.fullPage = false;
                CKEDITOR.instances[selector].setMode( 'source' );
                CKEDITOR.instances[selector].setData(selectorData, { callback: this.checkDirty });
            }
        }
    }
});